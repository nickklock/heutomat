#include <Wire.h>
#include "DS1307.h"
#include <multi_channel_relay.h>

Multi_Channel_Relay relay;
DS1307 clock;//define a object of DS1307 class

void setup() {
  
  setUpTime();
  setUpRelay();
}

void loop() {
  Serial.begin(9600);
  printTime();
  //Serial.println(clock.year, DEC);
}

void setUpTime(){
    clock.begin();
    clock.fillByYMD(2020, 12, 22); //Jan 19,2013
    clock.fillByHMS(22, 58, 30); //15:28 30"
    clock.fillDayOfWeek(TUE);//Saturday
    clock.setTime();//write time to the RTC chip
}

void printTime() {
    clock.getTime();
    int h = (clock.hour);
    int m = (clock.minute);
    int s = 0;
    if(h=22 && m=30 && s=0){
      relay.turn_on_channel(2); //Runter
      delay(20000); // warten bis ende
      relay.turn_off_channel(2);//Runter ende
      delay(20000); //fresszeit
      relay.turn_on_channel(3); //hoch
      delay(20000); //warten bis oben
      relay.turn_off_channel(3);//hoch ende
    }
}

void setUpRelay(){
      relay.begin(0x11);
      relay.turn_on_channel(1); //Normal plus
}
